package com.saltboxgames.jscon.exceptions;

public class SParseException extends Exception {

	public SParseException() {
		// TODO Auto-generated constructor stub
	}

	public SParseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SParseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SParseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
