package com.saltboxgames.jscon.exceptions;

public class SconInvalidObjectException extends SconException {

	public SconInvalidObjectException() {
		// TODO Auto-generated constructor stub
	}

	public SconInvalidObjectException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public SconInvalidObjectException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public SconInvalidObjectException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public SconInvalidObjectException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
