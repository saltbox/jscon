package com.saltboxgames.jscon;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Queue;

import com.saltboxgames.jscon.comments.SCommentContainer;
import com.saltboxgames.jscon.conversion.SConversionContainer;
import com.saltboxgames.jscon.conversion.SConverter;
import com.saltboxgames.jscon.exceptions.SconException;

public class SArray extends SToken implements List<SToken>, Queue<SToken> {
	
	private LinkedList<SToken> tokenList;

	public SArray() {
		this.comments = new SCommentContainer(this);
		this.tokenList = new LinkedList<SToken>();
	}

	public SArray(Collection<SToken> collection) {
		this.tokenList = new LinkedList<SToken>(collection);
	}

	public SArray(List<SToken> list) {
		this.tokenList = new LinkedList<SToken>(list);
	}

	public SArray(SToken[] array) {
		this.tokenList = new LinkedList<SToken>(Arrays.asList(array));
	}

	public void add(int index, SToken sToken) {
		tokenList.add(index, sToken);
	}

	public boolean add(SToken sToken) {
		return tokenList.add(sToken);
	}
	
	public boolean addAll(Collection<? extends SToken> collection) {
		return tokenList.addAll(collection);
	}
	
	public boolean addAll(int index, Collection<? extends SToken> collection) {
		return tokenList.addAll(index, collection);
	}
	
	public boolean addBoolean(double myBoolean) throws SconException {
		return tokenList.add(new SValue(myBoolean));
	}

	public boolean addDouble(double myDouble) throws SconException {
		return tokenList.add(new SValue(myDouble));
	}
	
	public boolean addFloat(float myFloat) throws SconException {
		return tokenList.add(new SValue(myFloat));
	}
	
	public boolean addInt(int myInt) throws SconException {
		return tokenList.add(new SValue(myInt));
	}
	
	public boolean addLong(long myLong) throws SconException {
		return tokenList.add(new SValue(myLong));
	}
	
	public boolean addShort(short myShort) throws SconException {
		return tokenList.add(new SValue(myShort));
	}

	public boolean addString(String myString) throws SconException {
		return tokenList.add(new SValue(myString));
	}

	public void clear() {
		tokenList.clear();
	}

	public boolean contains(Object obj) {
		return tokenList.contains(obj);
	}

	public boolean containsAll(Collection<?> collection) {
		return tokenList.containsAll(collection);
	}

	public SToken element() {
		return tokenList.element();
	}

	public SToken get(int index) {
		return tokenList.get(index);
	}

	public Object get(int index, Class<?> myClass) throws SconException {
		return (Boolean) SConversionContainer.getInstance().deserialize(myClass, this.tokenList.get(index));
	}

	public Object get(int index, SConverter sConverter) throws SconException {
		return sConverter.deserialize(this.tokenList.get(index));
	}

	public boolean getBoolean(int index) throws SconException {
		return (Boolean) SConversionContainer.getInstance().deserialize(Boolean.class, this.tokenList.get(index));
	}

	public double getDouble(int index) throws SconException {
		return (Double) SConversionContainer.getInstance().deserialize(Double.class, this.tokenList.get(index));
	}

	public float getFloat(int index) throws SconException {
		return (Float) SConversionContainer.getInstance().deserialize(Float.class, this.tokenList.get(index));
	}

	public int getInt(int index) throws SconException {
		return (Integer) SConversionContainer.getInstance().deserialize(Integer.class, this.tokenList.get(index));
	}

	public long getLong(int index) throws SconException {
		return (Long) SConversionContainer.getInstance().deserialize(Long.class, this.tokenList.get(index));
	}

	public float getShort(int index) throws SconException {
		return (Short) SConversionContainer.getInstance().deserialize(Short.class, this.tokenList.get(index));
	}

	@Override
	public STokenType getSTokenType() {
		return STokenType.SARRAY;
	}

	public int indexOf(Object arg0) {
		return tokenList.indexOf(arg0);
	}

	public boolean isEmpty() {
		return tokenList.isEmpty();
	}

	public Iterator<SToken> iterator() {
		return tokenList.iterator();
	}

	public int lastIndexOf(Object obj) {
		return tokenList.lastIndexOf(obj);
	}

	public ListIterator<SToken> listIterator() {
		return tokenList.listIterator();
	}

	public ListIterator<SToken> listIterator(int index) {
		return tokenList.listIterator(index);
	}

	public boolean offer(SToken sToken) {
		return tokenList.offer(sToken);
	}

	public SToken peek() {
		return tokenList.peek();
	}

	public SToken poll() {
		return tokenList.poll();
	}

	public SToken remove() {
		return tokenList.remove();
	}

	public SToken remove(int index) {
		return tokenList.remove(index);
	}

	public boolean remove(Object obj) {
		return tokenList.remove(obj);
	}

	public boolean removeAll(Collection<?> collection) {
		return removeAll(collection);
	}

	public boolean retainAll(Collection<?> collection) {
		return tokenList.retainAll(collection);
	}

	public void set(int index, Object obj) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.set(obj);;
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public void set(int index, Object obj, SConverter converter) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.set(converter, obj);
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public SToken set(int index, SToken sToken) {
		return tokenList.set(index, sToken);
	}

	public void setBoolean(int index, boolean myBoolean) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setBoolean(myBoolean);
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public void setDouble(int index, double myDouble) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setDouble(myDouble);
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public void setFloat(int index, float myFloat) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setFloat(myFloat);
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	// techno in je kanus!

	public void setInt(int index, int myInt) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setInt(myInt);
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public void setLong(int index, long myLong) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setLong(myLong);
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public void setNull(int index) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setNull();
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public void setShort(int index, short myShort) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setShort(myShort);
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public void setString(int index, String myString) throws SconException {
		SToken sToken = this.tokenList.get(index);
		if (sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setString(myString);
		} else {
			throw new SconException(
					"The SToken on index " + index + " is not from type SValue and can't be set a value.");
		}
	}

	public int size() {
		return tokenList.size();
	}

	public List<SToken> subList(int beginAt, int endAt) {
		return tokenList.subList(beginAt, endAt);
	}

	public Object[] toArray() {
		return tokenList.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return tokenList.toArray(a);
	}
	
	public Object tryGet(int index, Class<?> myClass) {
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGet(myClass);
			}
		}
		return null;
	}
	
	public Object tryGet(int index, SConverter sConverter) {
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGet(sConverter);
			}
		}
		return null;
	}

	public Boolean tryGetBoolean(int index) {
		// my radio believe me, I like it loud.
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGetBoolean();
			}
		}
		return null;
	}

	public Double tryGetDouble(int index) {
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGetDouble();
			}
		}
		return null;
	}

	public Float tryGetFloat(int index) {
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGetFloat();
			}
		}
		return null;
	}

	public Integer tryGetInt(int index) {
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGetInt();
			}
		}
		return null;
	}

	public Long tryGetLong(int index) {
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGetLong();
			}
		}
		return null;
	}

	public Short tryGetShort(int index) {
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGetShort();
			}
		}
		return null;
	}

	public SToken tryGetSToken(int index) {
		if (index < this.tokenList.size()) {
			return this.tokenList.get(index);
		}
		return null;
	}

	public String tryGetString(int index) {
		if (index < this.tokenList.size()) {
			SToken sToken = this.tokenList.get(index);
			if (sToken instanceof SValue) {
				return ((SValue) sToken).tryGetString();
			}
		}
		return null;

	}

	@Override
	public void setNull() {
		this.tokenList = new LinkedList<SToken>();
	}
}
