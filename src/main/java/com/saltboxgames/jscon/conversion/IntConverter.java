package com.saltboxgames.jscon.conversion;

import com.saltboxgames.jscon.SToken;
import com.saltboxgames.jscon.SValue;
import com.saltboxgames.jscon.SValueType;
import com.saltboxgames.jscon.exceptions.*;

public class IntConverter extends SConverter {

	public IntConverter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object deserialize(SToken sToken) throws SconException {
		if (!(sToken instanceof SValue)) {
			throw new SconSTokenException();
		}
		try {
			SValue sValue = ((SValue) sToken);
			String rawValue = sValue.getRawVaule();
			System.out.println(rawValue + "peekaboo");
			if(rawValue== null)
			{
				rawValue = "";
			}
			if (rawValue.startsWith("0x")) {
				return Integer.parseInt(rawValue.substring(2), 16);
			} else if (rawValue.startsWith("0b")) {
				return Integer.parseInt(rawValue.substring(2), 2);
			} else {
				return Integer.parseInt(rawValue);
			}
		} catch (Exception e) {
			throw new SconException(e);
		}
	}

	@Override
	public String serialize(SToken sToken, Object object) throws SconException {
		if (!(object instanceof Integer) && !(sToken instanceof SValue)) {
			throw new SconInvalidObjectException();
		}
		System.out.println((Integer)object);
		try {
			SValue sValue = (SValue) sToken;
			SValueType type = (sValue.getSValueType());
			switch (type) {
			case BINARY:
				return "0b" + Integer.toBinaryString((Integer) object);
			case HEXADECIMAL:
				return "0x" + Integer.toHexString((Integer) object);
			case UNDEFINED:
			case NUMERAL:
			default:
				sValue.setSValueType(SValueType.NUMERAL);
				return object.toString();
			case STRING:
				throw new Exception ("Not implemented yet");
				//TODO: implement this.
			}
		} catch (Exception e) {
			throw new SconException(e);
		}
	}
}
