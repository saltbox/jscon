package com.saltboxgames.jscon.conversion;

import java.util.HashMap;

import com.saltboxgames.jscon.SToken;
import com.saltboxgames.jscon.exceptions.SconException;

public class SConversionContainer {

	private static SConversionContainer INSTANCE;

	private HashMap<Class<?>, SConverter> converters;

	public SConversionContainer() {
		this.converters = new HashMap<Class<?>, SConverter>();
		converters.put(Double.class, new DoubleConverter());
		converters.put(Float.class, new FloatConverter());
		converters.put(Integer.class, new IntConverter());
		converters.put(ShortConverter.class, new ShortConverter());
		converters.put(String.class,  new StringConverter());
	}

	public String serialize(SToken sToken, Object obj) throws SconException {
		try {
			return this.converters.get(obj.getClass()).serialize(sToken, obj);
		} catch (Exception e) {
			throw new SconException(e);
		}
	}
	
	public String serialize(SConverter sConverter, SToken sToken, Object obj) throws SconException {
		try {
			return sConverter.serialize(sToken, obj);
		} catch (Exception e) {
			throw new SconException(e);
		}
	}


	public Object deserialize(Class<?> theClass, SToken sToken) throws SconException {
		try {
			return this.converters.get(theClass).deserialize(sToken);
		} catch (Exception e) {
			throw new SconException(e);
		}
	}
	
	public Object deserialize(SConverter sConverter, SToken sToken) throws SconException {
		try {
			return sConverter.deserialize(sToken);
		} catch (Exception e) {
			throw new SconException(e);
		}
	}

	public static SConversionContainer getInstance() {
		if (SConversionContainer.INSTANCE == null)
		{
			SConversionContainer.INSTANCE = new SConversionContainer();
		}
		
		return SConversionContainer.INSTANCE;
	}

	public static void setInstance(SConversionContainer container) throws SconException{
		if(container == null)
		{
			throw new SconException(new NullPointerException());
		}
		SConversionContainer.INSTANCE = container;
	}
	
}
