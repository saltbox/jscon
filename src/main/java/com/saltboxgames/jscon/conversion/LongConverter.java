package com.saltboxgames.jscon.conversion;

import com.saltboxgames.jscon.SToken;
import com.saltboxgames.jscon.SValue;
import com.saltboxgames.jscon.SValueType;
import com.saltboxgames.jscon.exceptions.SconException;
import com.saltboxgames.jscon.exceptions.SconInvalidObjectException;
import com.saltboxgames.jscon.exceptions.SconSTokenException;

public class LongConverter extends SConverter {

	public LongConverter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object deserialize(SToken sToken) throws SconException {
		if (!(sToken instanceof SValue)) {
			throw new SconSTokenException();
		}
		try {
			SValue sValue = ((SValue) sToken);
			if (sValue.getRawVaule().startsWith("0x")) {
				return Long.parseLong(sValue.getRawVaule().substring(2), 16);
			} else if (sValue.getRawVaule().startsWith("0b")) {
				return Long.parseLong(sValue.getRawVaule().substring(2), 2);
			} else {
				return Long.parseLong(sValue.getRawVaule());
			}
		} catch (Exception e) {
			throw new SconException(e);
		}
	}

	@Override
	public String serialize(SToken sToken, Object object) throws SconException {
		if (!(object instanceof Long) && !(sToken instanceof SValue)) {
			throw new SconInvalidObjectException();
		}
		try {
			SValue sValue = (SValue) sToken;
			SValueType type = (sValue.getSValueType());
			switch (type) {
			case BINARY:
				return "0b" + Long.toBinaryString((Long) object);
			case HEXADECIMAL:
				return "0x" + Long.toHexString((Long) object);
			case STRING:
			case UNDEFINED:
			case NUMERAL:
			default:
				sValue.setSValueType(SValueType.NUMERAL);
				return Long.toString((Long) object);
			}
		} catch (Exception e) {
			throw new SconException(e);
		}
	}

}
