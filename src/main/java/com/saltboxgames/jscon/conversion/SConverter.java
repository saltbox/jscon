package com.saltboxgames.jscon.conversion;

import com.saltboxgames.jscon.SToken;
import com.saltboxgames.jscon.exceptions.SconException;

public abstract class SConverter {

	SToken sToken;
	
	public SConverter() {
	}
	
	public abstract Object deserialize(SToken sToken) throws SconException;
	
	public abstract String serialize(SToken sToken, Object object) throws SconException;
}
