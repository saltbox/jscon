package com.saltboxgames.jscon.conversion;

import com.saltboxgames.jscon.SToken;
import com.saltboxgames.jscon.SValue;
import com.saltboxgames.jscon.SValueType;
import com.saltboxgames.jscon.exceptions.SconException;
import com.saltboxgames.jscon.exceptions.SconInvalidObjectException;
import com.saltboxgames.jscon.exceptions.SconSTokenException;

public class ShortConverter extends SConverter {

	public ShortConverter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object deserialize(SToken sToken) throws SconException {
		if (!(sToken instanceof SValue)) {
			throw new SconSTokenException();
		}
		try {
			SValue sValue = ((SValue) sToken);
			if (sValue.getRawVaule().startsWith("0x")) {
				return Short.parseShort(sValue.getRawVaule().substring(2), 16);
			} else if (sValue.getRawVaule().startsWith("0b")) {
				return Short.parseShort(sValue.getRawVaule().substring(2), 2);
			} else {
				return Short.parseShort(sValue.getRawVaule());
			}
		} catch (Exception e) {
			throw new SconException(e);
		}
	}

	@Override
	public String serialize(SToken sToken, Object object) throws SconException {
		if (!(object instanceof Integer) && !(sToken instanceof SValue)) {
			throw new SconInvalidObjectException();
		}
		try {
			SValue sValue = (SValue) sToken;
			SValueType type = (sValue.getSValueType());
			switch (type) {
			case BINARY:
				// why do I have to do hacks? Java, get good!
				return "0b"+String.format("%016d", Integer.parseInt(Integer.toBinaryString((Short) object)));
			case HEXADECIMAL:
				// these are the things where people get fed-up with on java.
				// Get gud java!
				return "0x"+Integer.toHexString(((Short) object) & 0xffff);
			case STRING:
			case UNDEFINED:
			case NUMERAL:
			default:
				sValue.setSValueType(SValueType.NUMERAL);
				return Integer.toString((Integer) object);
			}
		} catch (Exception e) {
			throw new SconException(e);
		}
	}

}
