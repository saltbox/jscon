package com.saltboxgames.jscon;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.saltboxgames.jscon.comments.SCommentContainer;
import com.saltboxgames.jscon.conversion.SConversionContainer;
import com.saltboxgames.jscon.conversion.SConverter;
import com.saltboxgames.jscon.exceptions.SconException;

public class SObject extends SToken implements Map<String, SToken> {

	private LinkedHashMap<String, SToken> subTokens;

	public SObject(Map<String, SToken> map) {
		this.subTokens = new LinkedHashMap<String, SToken>(map);
		this.comments = new SCommentContainer(this);
	}

	public SObject() {
		this.subTokens = new LinkedHashMap<String, SToken>();
		this.comments = new SCommentContainer(this);
	}

	public void clear() {
		subTokens.clear();
	}

	public boolean containsKey(Object arg0) {
		return subTokens.containsKey(arg0);
	}

	public boolean containsValue(Object arg0) {
		return subTokens.containsKey(arg0);
	}

	public Set<java.util.Map.Entry<String, SToken>> entrySet() {
		return subTokens.entrySet();
	}

	public SToken get(Object key) {
		return subTokens.get(key);
	}

	@Override
	public STokenType getSTokenType() {
		return STokenType.SOBJECT;
	}

	public boolean isEmpty() {
		return subTokens.isEmpty();
	}

	public Set keySet() {
		return subTokens.keySet();
	}

	public SToken put(String key, SToken value) {
		return subTokens.put(key, value);
	}

	public void putAll(Map<? extends String, ? extends SToken> m) {
		subTokens.putAll(m);
	}

	public SToken remove(Object key) {
		return subTokens.remove(key);
	}

	public int size() {
		return subTokens.size();
	}

	public Integer tryGetInt(String key) {
		return ((SValue) this.subTokens.get(key)).tryGetInt();
	}

	public Collection<SToken> values() {
		return subTokens.values();
	}

	// There are 4 steps to hardcore!

	public void add(String key, SToken sToken) {
		this.subTokens.put(key, sToken);
	}

	// TODO: add method dependancies for this method.
	/*
	 * public void add(String key, Object obj) { this.subTokens.put(key,
	 * SConversionContainer.getInstance().serialize(obj))); }
	 */

	public void addBoolean(String key, double myBoolean) throws SconException {
		this.subTokens.put(key, new SValue(myBoolean));
	}

	public void addDouble(String key, double myDouble) throws SconException {
		this.subTokens.put(key, new SValue(myDouble));
	}

	public void addFloat(String key, float myFloat) throws SconException {
		this.subTokens.put(key, new SValue(myFloat));
	}

	public void addInt(String key, int myInt) throws SconException {
		this.subTokens.put(key, new SValue(myInt));
	}

	public void addLong(String key, long myLong) throws SconException {
		this.subTokens.put(key, new SValue(myLong));
	}

	public void addShort(String key, short myShort) throws SconException {
		this.subTokens.put(key, new SValue(myShort));
	}

	public void addString(String key, String myString) throws SconException {
		this.subTokens.put(key, new SValue(myString));
	}

	// Step 1 is the rythm and the baaassee druuumm! *powpowpowpow*

	public SToken get(String key) {
		return this.subTokens.get(key);
	}

	public Object get(String key, Class<?> myClass) throws SconException {
		return (Boolean) SConversionContainer.getInstance().deserialize(myClass, this.subTokens.get(key));
	}

	public Object get(String key, SConverter sConverter) throws SconException {
		return sConverter.deserialize(this.subTokens.get(key));
	}

	public boolean getBoolean(String key) throws SconException {
		return (Boolean) SConversionContainer.getInstance().deserialize(Boolean.class, this.subTokens.get(key));
	}

	public double getDouble(String key) throws SconException {
		return (Double) SConversionContainer.getInstance().deserialize(Double.class, this.subTokens.get(key));
	}

	public float getFloat(String key) throws SconException {
		return (Float) SConversionContainer.getInstance().deserialize(Float.class, this.subTokens.get(key));
	}

	public int getInt(String key) throws SconException {
		return (Integer) SConversionContainer.getInstance().deserialize(Integer.class, this.subTokens.get(key));
	}

	public long getLong(String key) throws SconException {
		return (Long) SConversionContainer.getInstance().deserialize(Long.class, this.subTokens.get(key));
	}

	public float getShort(String key) throws SconException {
		return (Short) SConversionContainer.getInstance().deserialize(Short.class, this.subTokens.get(key));
	}

	public SToken remove(String key) {
		return this.subTokens.remove(key);
	}

	public boolean remove(String key, SToken sToken) {
		return this.subTokens.remove(key, sToken);
	}

	// Step 2 is something that you don't forget. And that's the rythm of the
	// hiiiiii hat!

	// TODO: review this method and such
	/*
	 * 
	 * public void set(String key, Object obj) throws SconException { SToken
	 * sToken = this.tokenList.get(index); if (sToken instanceof SValue) {
	 * SValue sValue = (SValue) sToken; sValue.set(obj);; } else { throw new
	 * SconException( "The SToken on index " + index +
	 * " is not from type SValue and can't be set a value."); } }
	 */

	public void set(String key, Object obj, SConverter converter) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if (sToken !=null && sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.set(converter, obj);
			// TODO: make it so that you could also initiate to SObjects or
			// sArrays form this method.
		} else if(sToken == null){
			sToken = new SValue(converter, obj);
			this.subTokens.put(key, sToken);
		} else {
		
			throw new SconException(
					"The SToken on index " + key + " is not from type SValue and can't be set a value.");
		}
	}

	public SToken set(String key, SToken sToken) {
		this.subTokens.put(key,  sToken);
		return sToken;
	}

	public void setBoolean(String key, boolean myBoolean) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if (sToken !=null && sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setBoolean(myBoolean);
			// TODO: make it so that you could also initiate to SObjects or
			// sArrays form this method.
		} else if(sToken == null){
			this.subTokens.put(key, new SValue(myBoolean));
		} else {
			throw new SconException(
					"The SToken on index " + key + " is not from type SValue and can't be set a value.");
		}
	}

	public void setDouble(String key, double myDouble) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if (sToken !=null && sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setDouble(myDouble);
			// TODO: make it so that you could also initiate to SObjects or
			// sArrays form this method.
		} else if(sToken == null){
			this.subTokens.put(key, new SValue(myDouble));
		} else {
			throw new SconException(
					"The SToken on index " + key + " is not from type SValue and can't be set a value.");
		}
	}

	public void setFloat(String key, float myFloat) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if (sToken !=null && sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setFloat(myFloat);
			// TODO: make it so that you could also initiate to SObjects or
			// sArrays form this method.
		} else if(sToken == null){
			this.subTokens.put(key, new SValue(myFloat));
		} else {
			throw new SconException(
					"The SToken on index " + key + " is not from type SValue and can't be set a value.");
		}
	}

	public void setInt(String key, int myInt) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if (sToken !=null && sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setInt(myInt);
			// TODO: make it so that you could also initiate to SObjects or
			// sArrays form this method.
		} else if(sToken == null){
			this.subTokens.put(key, new SValue(myInt));
		} else {
			throw new SconException(
					"The SToken on index " + key + " is not from type SValue and can't be set a value.");
		}
	}

	public void setLong(String key, long myLong) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if (sToken !=null && sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setLong(myLong);
			// TODO: make it so that you could also initiate to SObjects or
			// sArrays form this method.
		} else if(sToken == null){
			this.subTokens.put(key, new SValue(myLong));
		} else {
			throw new SconException(
					"The SToken on index " + key + " is not from type SValue and can't be set a value.");
		}
	}

	public void setNull(String key) throws SconException {
		SToken sToken = this.subTokens.get(key);
		sToken.setNull();
	}

	public void setShort(String key, short myShort) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if (sToken !=null && sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setShort(myShort);
			// TODO: make it so that you could also initiate to SObjects or
			// sArrays form this method.
		} else if(sToken == null){
			this.subTokens.put(key, new SValue(myShort));
		} else {
			throw new SconException(
					"The SToken on index " + key + " is not from type SValue and can't be set a value.");
		}
	}

	public void setString(String key, String myString) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if (sToken !=null && sToken instanceof SValue) {
			SValue sValue = (SValue) sToken;
			sValue.setString(myString);
			// TODO: make it so that you could also initiate to SObjects or
			// sArrays form this method.
		} else if(sToken == null){
			this.subTokens.put(key, new SValue(myString));
		} else {
			throw new SconException(
					"The SToken on index " + key + " is not from type SValue and can't be set a value.");
		}
	}

	@Override
	public void setNull() {
		this.subTokens = new LinkedHashMap<String, SToken>();
		
	}

	public Object getOrDefault(String key, Class<?> myClass, Object defaultObject) throws SconException
	{
		SToken sToken = this.subTokens.get(key);
		if(sToken == null || !sToken.isEmpty()){
			return defaultObject;
		}
		return SConversionContainer.getInstance().deserialize(myClass, sToken);
	}

	public int getOrDefaultInt(String key, int defaultInt) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if(sToken == null || !sToken.isEmpty()){
			return defaultInt;
		}
		return (int)(Integer)SConversionContainer.getInstance().deserialize(Integer.class, sToken);
	}
	
	public double getOrDefaultDouble(String key, double defaultDouble) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if(sToken == null || !sToken.isEmpty()){
			return defaultDouble;
		}
		return (double)(Double)SConversionContainer.getInstance().deserialize(Double.class, sToken);
	}
	
	public long getOrDefaultLong(String key, long defaultLong) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if(sToken == null || !sToken.isEmpty()){
			return defaultLong;
		}
		return (long)(Long)SConversionContainer.getInstance().deserialize(Long.class, sToken);
	}
	
	public float getOrDefaultFloat(String key, float defaultFloat) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if(sToken == null || !sToken.isEmpty()){
			return defaultFloat;
		}
		return (float)(Float)SConversionContainer.getInstance().deserialize(Float.class, sToken);
	}
	
	public String getOrDefaultString(String key, String defaultString) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if(sToken == null || !sToken.isEmpty()){
			return defaultString;
		}
		return (String)SConversionContainer.getInstance().deserialize(String.class, sToken);
	}
	
	public boolean getOrDefaultBoolean(String key, boolean defaultBoolean) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if(sToken == null || !sToken.isEmpty()){
			return defaultBoolean;
		}
		return (boolean)(Boolean)SConversionContainer.getInstance().deserialize(Boolean.class, sToken);
	}
	
	public short getOrDefaultShort(String key, short defaultShort) throws SconException {
		SToken sToken = this.subTokens.get(key);
		if(sToken == null || !sToken.isEmpty()){
			return defaultShort;
		}
		return (short)(Short)SConversionContainer.getInstance().deserialize(Short.class, sToken);
	}
}