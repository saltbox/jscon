package com.saltboxgames.jscon;

public enum STokenType {
	SOBJECT,
	SVALUE,
	SARRAY
}
