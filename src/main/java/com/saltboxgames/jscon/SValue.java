package com.saltboxgames.jscon;

import com.saltboxgames.jscon.exceptions.*;
import com.saltboxgames.jscon.comments.SValueComment;
import com.saltboxgames.jscon.conversion.SConversionContainer;
import com.saltboxgames.jscon.conversion.SConverter;;

public class SValue extends SToken {

	private String value;
	private SValueType sValueType;

	public SValue() {
		init();
	}

	public SValue(double myDouble) throws SconException {
		init();
		this.setDouble(myDouble);
	}

	public SValue(float myFloat) throws SconException {
		init();
		this.setFloat(myFloat);
	}

	public SValue(int myInt) throws SconException {
		init();
		this.setInt(myInt);
	}

	public SValue(long myLong) throws SconException {
		init();
		this.setLong(myLong);
	}

	public SValue(short myShort) throws SconException {
		init();
		this.setShort(myShort);
	}

	public SValue(String string) throws SconException {
		init();
		this.setString(string);
	}

	public SValue(Object obj) throws SconException {
		init();
		this.set(obj);
	}
	
	public SValue(SConverter sConverter, Object obj) throws SconException {
		init();
		this.set(sConverter, obj);
	}

	public Object get(Class<?> myClass) throws SconException {
		return SConversionContainer.getInstance().deserialize(myClass, this);
	}
	
	public Object get(SConverter sConverter) throws SconException {
		return sConverter.deserialize(this);
	}

	public boolean getBoolean() throws SconException {
		return (Boolean) SConversionContainer.getInstance().deserialize(Boolean.class, this);
	}

	public double getDouble() throws SconException {
		return (Double) SConversionContainer.getInstance().deserialize(Double.class, this);
	}

	public float getFloat() throws SconException {
		return (Float) SConversionContainer.getInstance().deserialize(Float.class, this);
	}

	public int getInt() throws SconException {
		return (Integer) SConversionContainer.getInstance().deserialize(Integer.class, this);
	}

	public long getLong() throws SconException {
		return (Long) SConversionContainer.getInstance().deserialize(Long.class, this);
	}

	public String getRawVaule() {
		return value;
	}

	public float getShort() throws SconException {
		return (Short) SConversionContainer.getInstance().deserialize(Short.class, this);
	}

	@Override
	public STokenType getSTokenType() {
		return STokenType.SVALUE;
	}

	public String getString() throws SconException {
		return (String) SConversionContainer.getInstance().deserialize(String.class, this);
	}

	public SValueType getSValueType() {
		return this.sValueType;
	}

	private void init() {
		this.sValueType = SValueType.UNDEFINED;
		this.comments = new SValueComment(this);
	}

	public void set(Object obj) throws SconException {
		this.value = SConversionContainer.getInstance().serialize(this, obj);
	}
	
	public void set(SConverter sConverter, Object obj) throws SconException {
		this.value = sConverter.serialize(this, (Object) obj);
	}

	public void setBoolean(boolean myBoolean) throws SconException {
		this.value = SConversionContainer.getInstance().serialize(this, (Boolean) myBoolean);
	}

	public void setDouble(double myDouble) throws SconException {
		this.value = SConversionContainer.getInstance().serialize(this, (Double) myDouble);
	}

	public void setFloat(float myFloat) throws SconException {
		this.value = SConversionContainer.getInstance().serialize(this, (Float) myFloat);
	}

	public void setInt(int myInt) throws SconException {
		this.value = SConversionContainer.getInstance().serialize(this, (Integer) myInt);
	}

	public void setLong(long myLong) throws SconException {
		this.value = SConversionContainer.getInstance().serialize(this, (Long) myLong);
	}

	public void setNull() {
		this.value = null;
	}

	public void setShort(short myShort) throws SconException {
		this.value = SConversionContainer.getInstance().serialize(this, (Short) myShort);
	}

	public void setString(String myString) throws SconException {
		this.value = SConversionContainer.getInstance().serialize(this, myString);
	}

	public void setSValueType(SValueType sValueType) {
		this.sValueType = sValueType;
	}
	
	public Object tryGet(Class<?> myClass) {
		try {
			return SConversionContainer.getInstance().deserialize(myClass, this);
		} catch (SconException e) {
			return null;
		}
	}

	public Object tryGet(SConverter sConverter) {
		try {
			return sConverter.deserialize(this);
		} catch (SconException e) {
			return null;
		}
	}

	public Boolean tryGetBoolean() {
		try {
			return (Boolean) SConversionContainer.getInstance().deserialize(Boolean.class, this);
		} catch (SconException e) {
			return null;
		}
	}

	public Double tryGetDouble() {
		try {
			return (Double) SConversionContainer.getInstance().deserialize(Double.class, this);
		} catch (SconException e) {
			return null;
		}
	}

	public Float tryGetFloat() {
		try {
			return (Float) SConversionContainer.getInstance().deserialize(Float.class, this);
		} catch (SconException e) {
			return null;
		}
	}

	public Integer tryGetInt() {
		try {
			return (Integer) SConversionContainer.getInstance().deserialize(Integer.class, this);
		} catch (SconException e) {
			return null;
		}

	}

	public Long tryGetLong() {
		try {
			return (Long) SConversionContainer.getInstance().deserialize(Long.class, this);
		} catch (SconException e) {
			return null;
		}

	}

	public Short tryGetShort() {
		try {
			return (Short) SConversionContainer.getInstance().deserialize(Short.class, this);
		} catch (SconException e) {
			return null;
		}
	}

	public String tryGetString() {
		try {
			return (String) SConversionContainer.getInstance().deserialize(String.class, this);
		} catch (SconException e) {
			return null;
		}
	}
	
	public SValueComment getComments()
	{
		return (SValueComment)this.comments;
	}

	@Override
	public boolean isEmpty() {
		return this.value == null;
	}
}
