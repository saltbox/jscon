package com.saltboxgames.jscon;

import com.saltboxgames.jscon.comments.SCommentContainer;

public abstract class SToken {
	protected SCommentContainer comments;
	
	public SToken()
	{
		
	}
	
	public SCommentContainer getComment()
	{
		return this.comments;
	}
	
	public abstract STokenType getSTokenType();
	public abstract void setNull();
	public abstract boolean isEmpty();
}
