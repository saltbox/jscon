package com.saltboxgames.jscon;

public enum SValueType {
	STRING,
	NUMERAL,
	HEXADECIMAL,
	BINARY,
	UNDEFINED
}
