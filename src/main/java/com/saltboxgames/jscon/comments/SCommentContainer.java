package com.saltboxgames.jscon.comments;

import com.saltboxgames.jscon.SToken;

public class SCommentContainer {

	private String preComment;
	private String postComment;
	protected SToken sToken;
	
	public SCommentContainer(SToken sToken)
	{
		//TODO: initialize the internalComment array to the max size it can be according to the sToken.... wait, that can't work since SObjects may vary in size. Back to the drawing board.
	}
	
	public String getPostComment()
	{
		return this.postComment;
	}	
	
	public String getPreComment()
	{
		return this.preComment;
	}
	
	public void setPostComment(String postComment)
	{
		this.postComment = postComment;
	}
	
	public void setPreComment(String preComment)
	{
		this.preComment = preComment;
	}
}
