package com.saltboxgames.jscon.comments;

import com.saltboxgames.jscon.SToken;
import com.saltboxgames.jscon.SValue;

public class SValueComment extends SCommentContainer {

	private String internalComment;

	public SValueComment(SValue sValue) {
		super(sValue);
		this.internalComment = "";
	}

	public void setInternalComment(String comment) {
		this.internalComment = comment;
	}

	public String getInternalComment() {
		return this.internalComment;
	}
}