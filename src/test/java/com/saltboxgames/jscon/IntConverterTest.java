package com.saltboxgames.jscon;

import static org.junit.Assert.*;

import org.junit.*;

import org.junit.Test;

import com.saltboxgames.jscon.conversion.IntConverter;
import com.saltboxgames.jscon.exceptions.SconException;

public class IntConverterTest {

	public IntConverterTest() {
		// TODO Auto-generated constructor stub
	}

	@Test
	public void intConverterTest() throws SconException
	{
		int number = 13456673;
		SValue sValue = new SValue(number);
		IntConverter converter = new IntConverter();
		assertEquals(((int)((Integer)converter.deserialize(sValue))), number);
	}
}
